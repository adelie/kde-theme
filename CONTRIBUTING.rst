=========================================
 Contribution Guide for Adélie KDE Theme
=========================================
:Author:
  * **A. Wilcox**, documentation writer
:Copyright:
  © 2023 Adélie Linux Team.  NCSA open source licence.



Contributing Changes
====================

This section describes the usual flows of contribution to this repository.
For a detailed description of how to contribute to Adélie Linux, review the
Handbook_.

.. _Handbook: https://help.adelielinux.org/html/devel/


GitLab Merge Requests
`````````````````````

#. If you do not already have a GitLab account, you must create one.

#. Create a *fork* of the Horizon repository.  For more information, consult
   the GitLab online documentation.

#. Clone your forked repository to your computer.

#. Make your changes.

#. Test your changes to ensure they are correct.

#. Add (or remove) changed files using ``git add`` and ``git rm``.

#. Commit your changes to the tree using the commands ``git commit -S`` and
   ``git push``.

#. Visit your forked repository in a Web browser.

#. Choose the *Create Merge Request* button.

#. Review your changes to ensure they are correct, and then submit the form.
