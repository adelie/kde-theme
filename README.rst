=============================
 README for Adélie KDE Theme
=============================
:Authors:
 * **A. Wilcox**, principal author
 * **Zach van Rijn**, wallpapers
:Status:
 Production
:Copyright:
 © 2023 Adélie Linux and contributors.


This repository contains the KDE themes used for Adélie Linux.
