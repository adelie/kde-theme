================================
 Changelog for Adélie KDE Theme
================================
:Author:
  * **A. Wilcox**, documentation writer
:Copyright:
  © 2023 Adélie Linux and contributors.



1.0 (2023-12-08)
================

Initial release.
